CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Recommended Modules
* Installation
* Configuration
* Additional Licenses
* Maintainers

INTRODUCTION
------------

The Laces theme and the supporting modules Laces Base and Laces Block (coming soon)
use Bootstrap 5 to create a website that works with the Layout Builder ecosystem.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/laces

* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/laces

REQUIREMENTS
------------

This theme requires the following modules:

* [Twig Tweak] (https://www.drupal.org/project/twig_tweak)

RECOMMENDED MODULES
-------------------

* [Laces Base] (https://www.drupal.org/project/laces_base)
* [Laces Block] Coming soon

INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/node/895232/ for further information.

There is a sub-theme example that you can copy to your themes/custom
directory. See the README.md file in the subtheme_starterkits direcory
for additional information.

CONFIGURATION
-------------

'Laces' provides theming for edit pages and the 'Content' administration pages. On the Appearance
page uncheck 'Use the administration theme when editing or creating content.'

ADDITIONAL LICENSES
-------------------
The MIT License (MIT)

Copyright (c) 2011-2020 Twitter, Inc.

Copyright (c) 2011-2020 The Bootstrap Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

MAINTAINERS
-----------

* John Rasmussen (bigbaldy) - https://drupal.org/u/bigbaldy
