CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

This directory contains sub-theme templates that can be copied to your themes/custom
directory.

#### Sub-themes

* sublaces - A simple example to start from.

REQUIREMENTS
------------

See requirements in the README.md file in each individual sub-theme.

INSTALLATION
------------

Copy the sub-theme directory to your themes/custom directory. Then change the sub-theme
file THEMENAME.REMOVEinfo.yml to THEMENAME.info.yml. See the README.md file in
each individual sub-theme for additional installation information.

CONFIGURATION
-------------

Configuration may be different for each sub-theme. Check the README.md file in each
sub-theme for additional configuration options.

MAINTAINERS
-----------

* John Rasmussen (bigbaldy) - https://drupal.org/u/bigbaldy
