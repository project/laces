CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Recommended Modules
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

'sublaces' is a simple example sub-theme for the Laces theme (https://drupal.org/project/laces.)
It can be used as a starting point for customizing the Laces theme for your own website.

Included:
- A css addition for formatting lists.
- Template overrides for the eu_cookie_compliance module.

REQUIREMENTS
------------

This theme requires the following theme:

* [Laces] (https://www.drupal.org/project/laces)

RECOMMENDED MODULES
-------------------

See the recommended modules for the *Laces* theme.

INSTALLATION
------------

Install as you would normally install a custom Drupal theme. Copy the sublaces directory to your web/themes/custom
directory then rename the file 'sublaces.REMOVEinfo.yml' to 'sublaces.info.yml'.

CONFIGURATION
-------------

Make modifications to the *sublaces* theme that you copied to web/themes/custom/sublaces.

MAINTAINERS
-----------

* John Rasmussen (bigbaldy) - https://drupal.org/u/bigbaldy
